from datetime import datetime
import os
import time
from camera import Camera
import json

from flask import Flask, request, render_template, send_file

app = Flask(__name__)

photo_idx = 0
time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


if not os.path.exists(time):
    os.mkdir(time)

config = dict()
for filename in os.scandir("./config"):
    if filename.is_file():
        config[filename.name[:-5]] = json.load(open(f'config/{filename.name}'))
cameras = {camera: Camera(**config[camera]) for camera in config}

for name, camera in cameras.items():
    camera.start()

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.form:
        #print(request.form)
        if request.form['action'] == "updateState":
           for name, camera in cameras.items():
               camera.start()
        else:
            camera = cameras.get(request.form.get('Camera'))
            assert camera
            if request.form['action'] == "Stop":
                camera.stop()
            elif request.form['action'] == "Restart":
                pass
                for k, v in camera.__dict__.items():
                    if k in request.form.keys():
                        if k != 'cameraRes':
                            camera.__dict__[k] = float(request.form[k])
                        else:
                            camera.__dict__[k] = list(map(int, request.form[k].split('x')))
                camera.start()
            elif request.form['action'] == "Photo":
                camera.takePhoto(f"{time}/{photo_idx}_{request.form.get('Camera',None)}.jpg")
                #return send_file(f"{time}/{photo_idx}_{request.form.get('Camera')}.jpg", as_attachment=True)
        
    cameraState = {camera: cameras[camera].__dict__ for camera in cameras}
    cameraLimits = {camera: cameras[camera].limits for camera in cameras}
    return render_template('index.html', data={'camState': cameraState, 'limits': cameraLimits})
