# Camera Streaming

Installed on the Jetson, code for streaming camera feed to frontend.

### A tip for future maintainers: 
```
gst-inspect-1.0 --help
```

is your best friend. If you have any issue, run `gst-inspect-1.0 [NAME]`  -> EG: `gst-inspect-1.0 nvv4l2h264enc` to get information on how GST works.

ALSO IF you have to recompile janus-gateway, use `./configure --prefix=/opt/janus --disable-aes-gcm`

Note: `state.json` is split into separate JSON files for each camera. These individual files are in the `./config` directory. This is to prevent the file from becoming too large and causing issues with the frontend. We don't use `state.json` anymore, but it's still there for reference.