import subprocess
import shlex

class Camera:
    def __init__(
        self,
        bitrate,
        framerate,
        width,
        height,
        flip,
        interpolate,
        preset,
        enabled,
        saturation,
        contrast,
        brightness,
        cameraRes,
        crop,
        limits,
        device,
        port
    ):
        """Camera class to store camera settings

        Args:

        bitrate: bitrate of the camera
        framerate: framerate of the camera
        width: width of the camera frame
        height: height of the camera frame
        flip: flip direction in post-processing
        interpolate: interpolation method for up/downscaling
        preset: preset for the camera
        enabled: whether the camera is enabled
        saturation: saturation of the camera
        contrast: contrast of the camera
        brightness: brightness of the camera
        cameraRes: resolution of the camera
        crop: crop values (left,right,top,bottom)
        limits: limits of the camera
        device: path to camera input
        """

        self.bitrate = bitrate
        self.framerate = framerate
        self.width = width
        self.height = height
        self.flip = flip
        self.interpolate = interpolate
        self.preset = preset
        self.enabled = enabled
        self.saturation = saturation
        self.contrast = contrast
        self.brightness = brightness
        self.cameraRes = cameraRes
        self.crop = crop
        self.limits = limits
        self.device = device
        self.port = port

        assert len(self.cameraRes) == 2, "cameraRes must be a list of length 2 (width,height)"
        assert len(self.crop) == 4, "crop must be a list of length 4 (left,right,top,bottom)"

        self.cropLeft = crop[0]
        self.cropRight = crop[1]
        self.cropTop = crop[2]
        self.cropBottom = crop[3]

        self.process = None

    def __str__(self):
        return f"Camera: {self.__dict__}"

    def get_src(self, photo: bool = False):
        """Get the source"""
        src_string = (
            f"v4l2src device={self.device} io-mode=2 !"
            f"image/jpeg,width={self.cameraRes[0]},height={self.cameraRes[1]},framerate={30 if photo else min(self.limits['x'.join(str(r) for r in self.cameraRes)], key=lambda x:abs(x-self.framerate))}/1 !"
            f"nvjpegdec !"
            f"video/x-raw"
        )

        return src_string

    def start(self):
        """Start the camera"""
        if self.process:
            self.stop()
        self.getCameraString()
        self.process = subprocess.Popen(shlex.split(f"gst-launch-1.0 -e -vv {self.getCameraString()}"))

    def stop(self):
        """Stop the camera"""
        if self.process:
            self.process.send_signal(2)
        self.process = None

    def takePhoto(self, path: str):
        """Take a picture"""
        flag = self.process == None
        self.stop()
        subprocess.run(shlex.split(
            f"gst-launch-1.0 -e {self.get_src(photo=True)} !"      # Open GST with camera
            f"videoconvert !"                                      # convert video (automaticly finds right settings for next element)
            f"jpegenc idct-method=2 quality=100 snapshot=true !"   # Encode just 1 jepeg with max quality.
            f"filesink location={path}"   # Save in file location
        ))
        if not flag:
            self.start()

    def getCameraString(self):
        """Get string to run camera"""
        if self.enabled == 0:
            return ""
        theString =(
            f"{self.get_src()} !" # f"{vid_src[camera] if applicationState['limit_USB_bandwidth'] else vid_src_no_lim[camera]} ! "  # Start GST with camera
            f"videorate drop-only=true ! " # Change video rate, dont allow higher framerate than camera allows
            f"'video/x-raw,framerate={self.framerate:.0f}/1' ! " # framerate to change to
            f"videobalance saturation={self.saturation} contrast={self.contrast} brightness={self.brightness} !" # Change color and stuffs
            f"queue leaky=downstream max-size-buffers=1 ! " # If the encoder cant keep up, drop frames. Also does some threading magic
            f"timeoverlay time-mode=2 !"
            f"nvvidconv flip-method={self.flip:.0f} interpolation-method={self.interpolate:.0f} left={self.cropLeft:.0f} right={self.cropRight:.0f} top={self.cropTop:.0f} bottom={self.cropBottom:.0f} ! " # convert and scale video
            f"'video/x-raw(memory:NVMM),width={self.width:.0f},height={self.height:.0f}' ! " # move to graphics memory, size to scale to
            f"nvv4l2h264enc control-rate=0 bitrate={self.bitrate:.0f} preset-level={self.preset:.0f} ! " # encode to H264 at bitrate and with preset
            f"rtph264pay config-interval=1 !" # Wrap it in a RTP payload, send config info every second
            f"udpsink host=127.0.0.1 port={self.port} force-ipv4=true sync=false " # send it out out as a UDP packet to Janus
        )
        return theString
